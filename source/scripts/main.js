var navbarHeight = $('.navbar').outerHeight();

$(document).ready(function () {
   $("a.smoothscroll[href^='#']").on('click', function (e) {
        e.preventDefault();

        if ($(".navbar-toggler:visible").hasClass("collapsed")) {
            // Closed
        } else {
            // Open
            $(".navbar-toggler:visible").click();
        }

        var hash = this.hash;
        var offset = 0;
        if (hash) {
            offset = $(hash).offset().top;
            offset -= (navbarHeight + 20);
        }

        $('html, body').animate({
            scrollTop: offset
        }, 1000, function () {});
    });
});