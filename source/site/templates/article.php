<?php
  snippet('header');
  snippet('menu');
?>

<div id="article" class="page-outer" style="background-image: url(<?= $page->articleImage()->toFile()->url() ?>)">
  <div class="page-inner">
    <article>
      <div class="container">
        <div class="row text-center justify-content-center">
          <div class="col-12 col-lg-8">
            <div class="article__title">
              <h1><strong><?= $page->articleTitle() ?></strong></h1>
            </div>
          </div>
        </div>
        <div class="row justify-content-center text-justify">
          <div class="col-12 col-lg-8">
            <div class="article__text">
              <?= $page->articleText()->kirbytext() ?>
            </div>
          </div>
        </div>
      </div>
    </article>
  </div>
</div>

<?php
  snippet('footer');
?>