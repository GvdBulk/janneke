<?php

snippet('header');
snippet('menu');

?>

<div id="contact" class="page-outer" style="background-image: url(<?= $page->pageImage()->toFile()->url() ?>)">
  <div class="page-inner">
    <div class="container">
      <div class="col-12 offset-md-5 col-md-7">
        <div class="home__main-text">
          <div class="contact-title text-center mb-5">
            <h1>Contact</h1>
          </div>
          <form id="contact-form" method="post" action="<?= $page->url() ?>">
            <div class="form-group row">
              <label for="name" class="col-2">Naam*</label>
              <div class="col-10">
                <input type="text" class="form-control" id="name" name="name" value="" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="email" class="col-2">Email*</label>
              <div class="col-10">
                <input type="email" class="form-control" id="email" name="email" value="" required>
              </div>
            </div>
            <div class="form-group row">
              <label for="site" class="col-2">Site</label>
              <div class="col-10">
                <input type="text" class="form-control" id="site" name="site" value="">
              </div>
            </div>
            <div class="form-group row">
              <label for="message" class="col-12">Bericht</label>
              <div class="col-12">
                <textarea id="message" name="message" cols="30" rows="10" class="form-control"></textarea>
              </div>
            </div>
            <?php if($success): ?>
            <div class="row justify-content-center">
              <div class="col-8 success text-center mt-3">
                <?= $page->successMessage()->kirbytext() ?>
              </div>
            </div>
            <?php elseif(isset($alert['error'])): ?>
            <div class="row justify-content-center">
              <div class="col-8 error text-center mt-5">
                <?= $alert['error'] ?>
                <?= $page->errorMessage()->kirbytext() ?>
              </div>
            </div>
            <?php else: ?>
            <input type="submit" name="submit" value="Verzenden" style="float: right">
            <?php endif ?>
        </div>
        </form>
        <div class="home__quote">
          <div class="home__quote-text font-parisienne my-auto text-center">
            <?= $page->contactQuote() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php

snippet('footer');

?>