<?php

snippet('header');
snippet('menu');

?>

<div id="home" class="page-outer" style="background-image: url(<?= $page->pageImage()->toFile()->url() ?>)">
  <div class="page-inner">
    <div class="container">
      <div class="col-12 offset-md-5 col-md-7">
        <div class="home__main-text text-md-center text-justify">
          <?= $page->homepageText()->kirbytext() ?>
          <div class="home__quote">
            <div class="home__quote-text font-parisienne my-auto text-center">
              <?= $page->homepageQuote() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php

snippet('footer');

?>