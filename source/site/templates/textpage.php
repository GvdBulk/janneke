<?php
  snippet('header');
  snippet('menu');
?>

<div id="textpage" class="page-outer" style="background-image: url(<?= $page->pageImage()->toFile()->url() ?>)">
  <div class="page-inner">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <?= $page->pageText()->kirbytext() ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
  snippet('footer');
?>