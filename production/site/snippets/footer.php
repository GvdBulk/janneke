<?php $contact = $pages->filterBy('intendedTemplate', 'contact')->first(); ?>
<footer>
  <div class="logo">
    <img src="<?=$site->homepage()->url() ?>/assets/images/logo-footer.png" alt="">
  </div>
  <div class="container">
    <div class="row contact-info">
      <div class="col-12 text-center my-auto">
        <h1>Contact</h1>
        <a href="mailto: " <?= $contact->emailaddress() ?>><?= $contact->emailaddress() ?></a>
      </div>
    </div>
    <!-- <div class="row copyright">
      <div class="col-12 text-center my-auto">
        Copyright <?php echo date("Y"); ?> <a href="http://corinevandenbulk.nl">Corine.</a> & <a
          href="http://gertjanvdbulk.nl">GJ</a>
      </div>
    </div> -->
  </div>
</footer>

<?= js('assets/js/vendor.min.js') ?>
<?= js('assets/js/main.js') ?>