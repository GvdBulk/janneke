<!-- Default tags -->
<meta name="description"
  content="<?= $page->content()->has('articleCard') ? $page->articleCard() : $site->seoDescription() ?>">
<!-- Twitter Tags-->
<meta name="twitter:title"
  content="<?= $page->content()->has('articleTitle') ? $page->articleTitle() : $site->seoTitle() ?>">
<meta name="twitter:description"
  content="<?= $page->content()->has('articleCard') ? $page->articleCard() : $site->seoDescription() ?>">
<meta name="twitter:image"
  content="<?= $page->content()->has('articleImage') ? $page->articleImage()->toFile()->url() :  $site->seoImage()->toFile()->url() ?>">
<meta name="twitter:url" content="<?= $page->url() ?>">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="<?= $site->seoTwitterHandle() ?>">
<!-- Open graph Tags -->
<meta property="og:site_name" content="<?= $site->title() ?>">
<meta property="og:title"
  content="<?= $page->content()->has('articleTitle') ? $page->articleTitle() : $site->seoTitle() ?>">
<meta property="og:description"
  content="<?= $page->content()->has('articleCard') ? $page->articleCard() : $site->seoDescription() ?>">
<meta property="og:image"
  content="<?= $page->content()->has('articleImage') ? $page->articleImage()->toFile()->url() :  $site->seoImage()->toFile()->url() ?>">
<meta property="og:url" content="<?= $page->url() ?>">
<meta property="og:type" content="website">