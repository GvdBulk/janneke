<?php $homepage = $pages->filterBy('intendedTemplate', 'home')->first(); ?>
<nav id="menu" class="navbar fixed-top navbar-expand-lg navbar-dark nav-bg-black">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="fas fa-bars"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav nav-bg-black mx-auto">
      <a class="nav-item nav-item-uppercase nav-item-bold nav-link px-3 px-xl-3 py-3 py-lg-2 active smoothscroll"
        href="<?= $homepage->url() ?>"><?= $site->menuHome() ?></a>
      <span class="nav-item-seperator nav-item-bold py-1 py-lg-2 d-none d-lg-block">|</span>
      <a class="nav-item nav-item-uppercase nav-item-bold nav-link px-3 px-xl-3 py-3 py-lg-2 smoothscroll"
        href="<?= $homepage->url() ?>/hoe-zit-het-met"><?= $site->menuHowAbout() ?></a>
      <span class="nav-item-seperator nav-item-bold py-1 py-lg-2 d-none d-lg-block">|</span>
      <a class="nav-item nav-item-uppercase nav-item-bold nav-link px-3 px-xl-3 py-3 py-lg-2 smoothscroll"
        href="<?= $homepage->url() ?>/aan-de-slag"><?= $site->menuGetToIt() ?></a>
      <span class="nav-item-seperator nav-item-bold py-1 py-lg-2 d-none d-lg-block">|</span>
      <a class="nav-item nav-item-uppercase nav-item-bold nav-link px-3 px-xl-3 py-3 py-lg-2 smoothscroll"
        href="<?= $homepage->url() ?>/het-pensioen-van"><?= $site->menuRetirement() ?></a>
      <span class="nav-item-seperator nav-item-bold py-1 py-lg-2 d-none d-lg-block">|</span>
      <a class="nav-item nav-item-uppercase nav-item-bold nav-link px-3 px-xl-3 py-3 py-lg-2 smoothscroll"
        href="<?= $homepage->url() ?>/mijn-verhaal"><?= $site->menuAboutMe() ?></a>
      <span class="nav-item-seperator nav-item-bold py-1 py-lg-2 d-none d-lg-block">|</span>
      <a class="nav-item nav-item-uppercase nav-item-bold nav-link px-3 px-xl-3 py-3 py-lg-2 smoothscroll"
        href="<?= $homepage->url() ?>/contact"><?= $site->menuContact() ?></a>
    </div>
  </div>
  <div class="menu-logo">
    <img src="<?= $homepage->url() ?>/assets/images/logo-header.png" alt="">
  </div>
</nav>