<?php
return function($kirby, $page) {

    $alert = null;
    $test = $page->emailaddress();

    if($kirby->request()->is('POST') && get('submit')) {

        $data = [
            'name'  => get('name'),
            'email' => get('email'),
            'site'  => get('site'),
            'message' => get('message')
        ];

        $test = $data['message'];

        try {
            $kirby->email([
                'template' => 'contactemail',
                'from'     => 'contactform@janneke.com',
                'replyTo'  => $data['email'],
                'to'       => $page->emailaddress()->value(),
                'subject'  => esc($data['name']) . ' heeft je contact formulier ingevuld',
                'data'     => [
                    'message' => esc($data['message']),
                    'sender' => esc($data['name']),
                    'email' => esc($data['email'])
                ]
            ]);

        } catch (Exception $error) {
            
            if(option('debug')):
                $alert['error'] = 'The form could not be sent: <strong>' . $error->getMessage() . '</strong>';
            else:
                $alert['error'] = 'The form could not be sent!';
            endif;
        }

        // no exception occurred, let's send a success message
        if (empty($alert) === true) {
            $success = $page->successMessage()->kirbytext();
            $data = [];
        }
    }

    return [
        'test'    => $test,
        'alert'   => $alert,
        'data'    => $data ?? false,
        'success' => $success ?? false
    ];
};