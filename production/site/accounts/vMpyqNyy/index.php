<?php

return [
    'email' => 'info@planmijnpensioen.nl',
    'language' => 'nl',
    'name' => 'Janneke',
    'role' => 'admin'
];