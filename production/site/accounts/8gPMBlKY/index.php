<?php

return [
    'email' => 'info@gertjanvdbulk.nl',
    'language' => 'nl',
    'name' => 'Admin',
    'role' => 'admin'
];