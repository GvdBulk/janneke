<?php
  snippet('header');
  snippet('menu');
?>
<div id="blog" class="page-outer">
  <div class="page-inner">
    <div class="container">
      <div class="row text-center">
        <div class="col-12">
          <div class="blogs__title">
            <h1><?= $page->pageTitle() ?></h1>
          </div>
        </div>
      </div>
      <?php if($articles->count() == 0): ?>
      <div class="row text-center">
        <div class="col-12">
          <div class="blogs__no-articles">
            Er zijn (helaas) nog geen artikelen in deze categorie...
          </div>
        </div>
      </div>
      <?php endif ?>
      <div class="row">
        <?php foreach($articles as $article): ?>
        <div class="card col-12 col-sm-6 col-lg-4">
          <a class="card-link" href="<?= $article->url() ?>">
            <div class="blog-card-img">
              <img src="<?= $article->articleImage()->toFile()->url() ?>" alt="article-image">
            </div>
            <div class="card-body">
              <h3 class="card-title"><?= $article->articleCardTitle() ?></h3>
              <p class="card-text"><?= $article->articleCard()->excerpt(100) ?></p>
            </div>
          </a>
        </div>
        <?php endforeach ?>
      </div>
    </div>
  </div>
</div>


<?php
  snippet('footer');
?>