<?php

Kirby::plugin('janneke/tags', [
    'tags' => [
        'quote' => require_once __DIR__ . '/tags/quote.php',
        'newline' => require_once __DIR__ . '/tags/newline.php',
        'fancy' => require_once __DIR__ . '/tags/fancy.php'
    ]
]);